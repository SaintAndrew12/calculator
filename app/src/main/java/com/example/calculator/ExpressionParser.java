package com.example.calculator;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Deque;
import java.util.List;
import java.util.StringTokenizer;

class ExpressionParser {
    private static String operators = "+-*/";
    private static String delimiters = "() " + operators;
    private static String allowedSymbols = "1234567890." + operators + delimiters;

    private static boolean isDelimiter(String token) {
        if (token.length() != 1) return false;
        return delimiters.contains(token);
    }

    private static boolean isOperator(String token) {
        if (token.equals("u-")) return true;
        return operators.contains(token);
    }

    private static int priority(String token) {
        if (token.equals("(")) return 1;
        if (token.equals("+") || token.equals("-")) return 2;
        if (token.equals("*") || token.equals("/")) return 3;
        return 4;
    }

    private static void actionForClosingBracket(Deque<String> stack, List<String> postfix) throws Exception {
        if (stack.peek() != null) {
            while (!stack.peek().equals("(")) {
                postfix.add(stack.pop());
                if (stack.isEmpty()) {
                    throw new Exception("Скобки не согласованы");
                }
            }
        } else throw new Exception("Скобки не согласованы");
    }

    private static void actionForDelimiter(String curr, String prev, Deque<String> stack, List<String> postfix) throws Exception{
        switch (curr) {
            case "(":
                stack.push(curr);
                break;
            case ")":
                actionForClosingBracket(stack, postfix);
                stack.pop();
                break;
            default:
                if (!curr.equals("-") && (isOperator(prev))) throw new Exception("Операторы не согласованы");
                else
                if (curr.equals("-") && (prev.equals("") || (isDelimiter(prev) && !prev.equals(")")))) {
                    // унарный минус
                    curr = "u-";
                } else {
                    while (!stack.isEmpty() && (priority(curr) <= priority(stack.peek()))) {
                        postfix.add(stack.pop());
                    }
                }
                stack.push(curr);
                break;
        }
    }

    private static void unloading(Deque<String> stack, List<String> postfix) throws Exception {
        while (!stack.isEmpty()) {
            if (isOperator(stack.peek())) postfix.add(stack.pop());
            else {
                throw new Exception("Скобки не согласованы");
            }
        }
    }

    static List<String> parse(String infix) throws Exception {
        for (Character c: infix.toCharArray()){
            if (!allowedSymbols.contains(c.toString())) throw new Exception("Недопустимый символ: " + c);
        }
        
        List<String> postfix = new ArrayList<>();
        Deque<String> stack = new ArrayDeque<>();
        StringTokenizer tokenizer = new StringTokenizer(infix, delimiters, true);
        String prev = "";
        String curr;
        while (tokenizer.hasMoreTokens()) {
            curr = tokenizer.nextToken();
            if (!tokenizer.hasMoreTokens() && isOperator(curr)) {
                throw new Exception("Некорректное выражение");
            }
            if (curr.equals(" ")) continue;
            if (isDelimiter(curr)) actionForDelimiter(curr, prev, stack, postfix);
            else {
                postfix.add(curr);
            }
            prev = curr;
        }

        unloading(stack, postfix);
        return postfix;
    }
}
