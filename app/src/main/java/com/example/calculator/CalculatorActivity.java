package com.example.calculator;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

public class CalculatorActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculator);

        final EditText inputExpression = findViewById(R.id.editInputExpression);
        final TextView answer = findViewById(R.id.answer);
        Button submit = findViewById(R.id.submit);

        inputExpression.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answer.setText("");
            }
        });

        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                answer.setTextColor(Color.BLACK);
                try {
                    String answerValue =
                            Calculator.
                            calculate(ExpressionParser.parse(
                                    inputExpression.
                                            getText().
                                            toString())).toString();
                    if (answerValue.endsWith(".0"))
                        answerValue = answerValue.substring(0, answerValue.length() - 2);
                    answer.setText(answerValue);
                } catch (Exception e) {
                    answer.setTextColor(Color.RED);
                    answer.setText(e.getMessage());
                }
            }
        });
    }
}
